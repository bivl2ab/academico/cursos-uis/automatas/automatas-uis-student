---
# Proyectos 2019-2. Autómatas y lenguajes formales. 

## Prof: Gustavo Garzón

---

# Lista de Proyectos
1. [Autómata finito determinista para el seguimiento del ensamblaje de un automóvil](#proy1)
2. [MT para detectar posibles pacientes con depresión](#proy2)
3. [Autómata Formal Determinista que acepta las contraseñas de un aplicativo móvil](#proy3)
4. [Maquina de Turing para calcular el factorial de un número](#proy4)
5. [Máquina de Turing que resuelve ecuaciones diferenciales lineales homogéneas con coeficientes constantes de segundo orden](#proy5)
6. [Implementación de una Máquina de Turing que solfea partituras](#proy6)
7. [Automata the coup the game](#proy7)
8. [Automatización del medio farmacéutico: Autofarma](#proy8)
9. [Chatbot asistente virtual para ayuda ante sintomatologia del coronavirus COVID-19](#proy9)
10. [ENCRIPTADO DE DATOS](#proy10)
11. [Anautómata: un autómata de cifrado](#proy11)
12. [Modelado de las rutas del sistema público de bicicletas de Bucaramanga](#proy12)
13. [Enigma's Machine](#proy13)
14. [Verificador de codigos de compra](#proy14)
15. [Validación Sudoku 4x4](#proy15)
16. [TRIQUI 3000](#proy16)
17. [Smart Semaphore](#proy17)
18. [SOLUCIÓN DE SUDOKUS EMPLEANDO TÉCNICAS DE AUTÓMATAS](#proy18)
19. [ChatPy: desarrollo de un chat en el lenguaje Python por medio de Sockets](#proy19)
20. [Monitoring and control of energy consumption](#proy20)
21. [Modelamiento de las rutas aéreas más frecuentadas partiendo desde la ciudad de Bogotá](#proy21)
---

## Autómata finito determinista para el seguimiento del ensamblaje de un automóvil <a name="proy1"></a>

**Autores:**
**- Sebatian Pérez López**
**- Edward Andres Sandoval**
**- Sebastian Flores Rojas**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180057-2180052-2180050.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180057-2180052-2180050/PROYECTO_FINAL__1_.ipynb) [(video)](https://www.youtube.com/watch?v=aodcB0wNkPA ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180057-2180052-2180050.pdf)

---

## MT para detectar posibles pacientes con depresión <a name="proy2"></a>

**Autores:**
**- Parra G. Carlos**
**- Porras G. Juan**
**- Hernández R. Paula**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180059-2172000-2180048.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180059-2172000-2180048/ProyectoAutomatas.ipynb) [(video)](https://www.youtube.com/watch?v=maASRUfVS6M) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180059-2172000-2180048.pdf)

---

## Autómata Formal Determinista que acepta las contraseñas de un aplicativo móvil <a name="proy3"></a>

**Autores:**
**- Arley David Velazco**
**- Jhon Anderson Ramirez Contreras**
**- Mario Andres Anaya Merchan**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180016-2180984-2180076.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180016-2180984-2180076/Trabajofinal-2.ipynb) [(video)](https://www.youtube.com/watch?v=kZXli12lgn0) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180016-2180984-2180076.pdf)

---

## Maquina de Turing para calcular el factorial de un número <a name="proy4"></a>

**Autores:**
**- Pereira Millán Camilo José**
**- Ribero Arciniegas José David**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2171845-2150965.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2171845-2150965/ProyectoAutomatas2.0.ipynb) [(video)](https://www.youtube.com/watch?v=9CxNTxFBhME) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2171845-2150965.pdf)

---

## Máquina de Turing que resuelve ecuaciones diferenciales lineales homogéneas con coeficientes constantes de segundo orden <a name="proy5"></a>

**Autores:**
**- Geison Alfredo Blanco Rodríguez**
**- Juan Sebastián Estupiñán Cobos**
**- Sebastián Rivera León**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180045-2180056-2180033.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180045-2180056-2180033/MT_ecuaciones_diferenciales_homogeneas_de_segundo_orden.ipynb) [(video)](https://www.youtube.com/watch?v=Ohj2J3HSHIs) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180045-2180056-2180033.pdf)

---

## Implementación de una Máquina de Turing que solfea partituras <a name="proy6"></a>

**Autores:**
**- Tatiana Marcela Flórez**
**- Gabriel André Ordoñez**
**- Juan Pablo Pérez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180041-2180070-2180072.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180041-2180070-2180072/Proyectofinal.ipynb) [(video)](https://www.youtube.com/watch?v=ro9cxb9G7bo) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180041-2180070-2180072.pdf)

---

## Automata the coup the game <a name="proy7"></a>

**Autores:**
**- Valentina Escobar Bueno**
**- Andrés Felipe Rojas Santos**
**- Frans Guillermo Taboada**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180032-2181819-2180021.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180032-2181819-2180021/PLAY.ipynb) [(video)](https://www.youtube.com/watch?v=Jb6XrYnQ_f0) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180032-2181819-2180021.pdf)

---

## Automatización del medio farmacéutico: Autofarma <a name="proy8"></a>

**Autores:**
**- Juan Pablo Claro Pérez**
**- Horacio Antonio Camacho Holguín**
**- Carlos Meza**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2181707-2180986-2182041.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2181707-2180986-2182041/TrabajoFinal.ipynb) [(video)](https://www.youtube.com/watch?v=N1CrUu7nN5c) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2181707-2180986-2182041.pdf)

---

## Chatbot asistente virtual para ayuda ante sintomatologia del coronavirus COVID-19 <a name="proy9"></a>

**Autores:**
**- María Alejandra Estévez Pinto**
**- Luis Miguel Rodriguez**
**- Jonathan Tovar**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2172968-2172004-2172027.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2172968-2172004-2172027/Proyecto_final_automata(1).ipynb) [(video)](https://www.youtube.com/watch?v=bwcg2X0FNi8) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2172968-2172004-2172027.pdf)

---

## ENCRIPTADO DE DATOS <a name="proy10"></a>

**Autores:**
**- Sebastian Contreras Ceballos**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2171993.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2171993/Proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=ChDp_gE58Ro) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2171993.pdf)

---

## Anautómata: un autómata de cifrado <a name="proy11"></a>

**Autores:**
**- Daniel León**
**- Daniel González**
**- Julian García Duarte**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180044-2181932-2180025.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180044-2181932-2180025/Anautomata.ipynb) [(video)](https://www.youtube.com/watch?v=lkHKWQ6mSkQ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180044-2181932-2180025.pdf)

---

## Modelado de las rutas del sistema público de bicicletas de Bucaramanga <a name="proy12"></a>

**Autores:**
**- Andrés Felipe Barajas Wellman**
**- Javier Andrés Carrillo Infante**
**- Hernando José Rojas Castro**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2171988-2173039-2172005.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2171988-2173039-2172005/Proyecto_Final1.ipynb) [(video)](https://www.youtube.com/watch?v=s87DjywUjRc) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2171988-2173039-2172005.pdf)

---

## Enigma's Machine <a name="proy13"></a>

**Autores:**
**- Juan Camilo Bayona Quesada**
**- Alvaro Jesús Martínez Vargas**
**- Cristian Leonardo Rueda Quintanilla**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2172916-2172024-2143115.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2172916-2172024-2143115/Enigmasmachine.ipynb) [(video)](https://www.youtube.com/watch?v=PsrbRz99mI4) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2172916-2172024-2143115.pdf)

---

## Verificador de codigos de compra <a name="proy14"></a>

**Autores:**
**- Julian Camacho**
**- Juan Quintero**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180055-2180063.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180055-2180063/proyecto_final(2).ipynb) [(video)](https://www.youtube.com/watch?v=OZraqP0D0Mk) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180055-2180063.pdf)

---

## Validación Sudoku 4x4 <a name="proy15"></a>

**Autores:**
**- Daniel Eduardo Ortiz Celis**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2171469.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2171469/automata.ipynb) [(video)](https://www.youtube.com/watch?v=TIOzpQLYVD8) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2171469.pdf)

---

## TRIQUI 3000 <a name="proy16"></a>

**Autores:**
**- Anderson González**
**- Camilo Marín**
**- Elsyn Vargas**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2172715-2172969-2172009.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2172715-2172969-2172009/Triqui3000.ipynb) [(video)](https://www.youtube.com/watch?v=CPmlUK8ybko) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2172715-2172969-2172009.pdf)

---

## Smart Semaphore <a name="proy17"></a>

**Autores:**
**- Liliam Paola Calderon Sarmiento**
**- Juan Diego Reyes**
**- Jose Luis Soto Soto**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180051-2180073-2181583.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180051-2180073-2181583/SmartSemaphore.ipynb) [(video)](https://www.youtube.com/watch?v=q49lOj4i5-M) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180051-2180073-2181583.pdf)

---

## SOLUCIÓN DE SUDOKUS EMPLEANDO TÉCNICAS DE AUTÓMATAS <a name="proy18"></a>

**Autores:**
**- Juan Sebastián Trujillo Tierradentro**
**- Javier Eduardo Tarazona Castellanos**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2160602-2172015.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2160602-2172015/Proyecto%20Automatas.ipynb) [(video)](https://www.youtube.com/watch?v=ByNRGWk-lIs) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2160602-2172015.pdf)

---

## ChatPy: desarrollo de un chat en el lenguaje Python por medio de Sockets <a name="proy19"></a>

**Autores:**
**- Edwar Plata**
**- Laura Jaimes**
**- Diego Landinez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2180022-2171450-2180036.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2180022-2171450-2180036/TrabajoFinal.ipynb) [(video)](https://www.youtube.com/watch?v=-UIF0SzwA98) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2180022-2171450-2180036.pdf)

---

## Monitoring and control of energy consumption <a name="proy20"></a>

**Autores:**
**- Nicolas Velasquez**
**- Daniel Lizcano**
**- Juan Esteban Gonzalez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2171065-2171979-2171991.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2171065-2171979-2171991/Proyecto%20Final.ipynb) [(video)](https://www.youtube.com/watch?v=3LqCoyHp8dg) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2171065-2171979-2171991.pdf)

---

## Modelamiento de las rutas aéreas más frecuentadas partiendo desde la ciudad de Bogotá <a name="proy21"></a>

**Autores:**
**- Angie Natalia Arias Gómez**
**- Ivan Andrés Mendoza**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2019-2/2172017-2171977.png" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2019-2/2172017-2171977/Proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=cph9KJt59NM) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2019-2/2172017-2171977.pdf)

---
