# Autómatas y Lenguajes Formales 2022-2


<!--## Máquina Virtual

Usaremos esta máquina virtual que tiene instalado un entorno Python Anaconda con Jupyter Notebooks, el cual se hace visible al ingresar en el navegador la URL: http://localhost:9999 una vez que la máquina arranca.


La máquina virtual puede descargarse aquí: https://drive.google.com/file/d/1zEGShPTQqZ_skcVoxF-lULoqEZJcQTc6/view?usp=sharing-->


## Google Colaboratory

Durante el periodo académico utilizaremos la plataforma Google Colaboratory para el desarrollo de talleres y parciales. El único requisito es que usted cuente con un correo @gmail.com, el cual le permitirá utilizar Google Drive, Google Docs y otras herramientas.

## Calificación

20% Talleres y quices<br/>
20% Parcial 1<br/>
20% Parcial 2<br/>
20% Parcial 3<br/>
20% Proyecto final<br/>



## Talleres

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada uno de los talleres se desarrollará en casa, dentro de las fechas establecidas en el cronograma.


## Parciales

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Durante los parciales, únicamente es posible utilizar los apuntes y notebooks del curso.


## Proyecto final
Se aceptarán grupos de máximo 3 estudiantes. Realizar el proyecto de forma individual es también aceptado.
- 10% Definición del tema y equipo de trabajo<br/>
Presentación corta (5 minutos, 4 o 5 diapositivas) que contenga: 1) Título del proyecto; 2) Integrantes; 3) Alcance (¿qué se espera obtener?); 4) Revisión del "estado del arte" (anális de los proyectos anteriores relacionados); 5) ¿Qué conceptos del curso pondrían en práctica en la realización del proyecto?

- 20% Implementación
- 20% Definición formal y diagramas de transiciones correspondientes
- 10% Presentación siguiendo la plantilla oficial del evento SystemsFest, en donde se muestre: título del proyecto, abstract (resumen), introducción, propuesta y/o resultados. Debe enviarse en formato PDF.<br>
- 20% Información complementaria:<br/>
Imagen PNG o JPG que contenga la siguiente información: título del proyecto e información de los estudiantes. Debe tener una foto de fondo que esté relacionada directamente con el tema de su proyecto.<br/><br/>
Video de **MÁXIMO 4 minutos** en donde se presenta el proyecto. Se debe enviar el archivo del video (formato *.mp4 en lo posible).<br/>
- 20% Sustentación y puntualidad en la fecha y hora asignada

Nota: todos estos ítems deberán ser alojados en un repositorio (github, gitlab, etc) de alguno de los estudiantes del grupo.


## Calendario y plazos

                        SESSION 1                  SESSION 2              SESSION SATURDAY

     W01 Mar13-Mar17    Intro                      Alfabetos y Lenguajes
     W02 Mar20-Mar24    AFD                        AFD
     W03 Mar27-Mar31    AFN                        Presentaciones
     ...................    SEMANA SANTA    .....................
     W04 Abr10-Abr14    Python                     Práctiva AFD               
     W05 Abr17-Abr21    Práctica AFN               Repaso y ejercicios        
     W06 Abr24-Abr28    Ejercicios                 AFN-e                      
     W07 May01-May05    Práctica AFN-e             PARCIAL I
     W08 May08-May12    Expresiones Regulares      Propiedades E.R.           
     W09 May15-May19    Conversión E.R. (a)        Conversión E.R. (b)        
     W10 May22-May26    Minimización               Gramáticas GLC (a)
     W11 May29-Jun02    Gramáticas GLC (b)         Gramáticas GLC (c)
     W12 Jun05-Jun09    PARCIAL II                 AP y MT
     W13 Jun12-Jun16    AP                         MT y presentaciones
     W14 Jun19-Jun23    AP, MT y presentaciones    Dudas y presentaciones
     ...................    VACACIONES    ..................... 
     W15 Jul10-Jul14    Ejercicios                 Sustentación Proyecto
     W16 Jul10-Jul14    Parcial III                        ...
                                     
     


     Abr 28 -           -> Registro primera nota
     May 12 -           -> Último día cancelación de semestre
     May 12 -           -> Último día cancelación de materias
     Jun 26 -           -> Inicio vacaciones 
     Jul 10 -           -> Reanudación de clases
     Jul 21 -           -> Finalización clase
     Jul 27 - Jul 28    -> Habilitaciones
     Jul 28 -           -> Registro calificaciones finales

    

<!--MT Multibanda Multicinta   TBA-->

<!-- Ver el Calendario academico 2019:
https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2019/acuerdoAcad064_2019.pdf-->

<!--[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2019/acuerdoAcad314-2019.pdf)-->

<!--[Calendario academico 2020-1](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad104_2020.pdf)-->

<!--[Calendario academico 2020-2](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad294_2020.pdf)-->

<!--[Calendario academico 2021-1](http://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad434_2020.pdf)-->

<!--[Calendario academico 211 del 2021](http://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2021/acuerdoAcad211_2021.pdf)-->

<!--[Calendario academico 277 del 2021](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2021/acuerdoAcad277_2021.pdf)-->



